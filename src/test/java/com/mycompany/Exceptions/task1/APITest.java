package com.mycompany.Exceptions.task1;

import static org.junit.Assert.*;

/**
 * Created by asus on 5/23/17.
 */
import org.junit.Test;

public class APITest {

    API api = new API();

    @Test
    public void testExceptionNoData() throws NoDataException, NoLocationFoundException, NoNetworkException, TimeOutException {
        Location location = new Location("Sofia", 1);
        boolean thrown = false;
        try {
            api.getWeather(location);
        } catch (NoDataException noData) {
            thrown = true;
        }

        assertTrue(thrown);

    }
    @Test
    public void testExceptionNoLocationFound() throws NoDataException, NoLocationFoundException, NoNetworkException, TimeOutException{
        Location location = new Location("Sofia", 2);
        boolean thrown = false;
        try {
            api.getWeather(location);
        } catch (NoLocationFoundException noLocationFound) {
            thrown = true;
        }

        assertTrue(thrown);

    }
    @Test
    public void testExceptionNoNetwork() throws NoDataException, NoLocationFoundException, NoNetworkException, TimeOutException{
        Location location = new Location("Sofia", 3);
        boolean thrown = false;
        try {
            api.getWeather(location);
        } catch (NoNetworkException noNetwork) {
            thrown = true;
        }

        assertTrue(thrown);

    }
    @Test
    public void testExceptionTimeOut() throws NoDataException, NoLocationFoundException, NoNetworkException, TimeOutException{
        Location location = new Location("Sofia", 4);
        boolean thrown = false;
        try {
            api.getWeather(location);
        } catch (TimeOutException timeOut) {
            thrown = true;
        }

        assertTrue(thrown);

    }


}