package com.mycompany.FileStreamsLambdas;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import com.mycompany.FileStreamsLambdas.Exceptions.*;
/**
 * Created by asus on 6/7/17.
 */
public class KeyValueParserTest {
    public final Path filePath = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/key-value.txt");
    @Test
    public void readFileTest() throws NotAValidPathException, NotAValidFileFormatException,NoPropertiesException, IOException
    {
        try {
            HashMap<String, String> expectPairs = new HashMap<>();
            expectPairs.put("key", "value");
            KeyValueParser parser = new KeyValueParser(filePath);
            Assert.assertEquals(expectPairs, parser.getKeyValuePairs());
        } catch (NotAValidPathException e){
            throw new NotAValidPathException("The path argument is not valid");
        } catch (NotAValidFileFormatException e){
            throw new NotAValidFileFormatException("The format of the file is not valid.Can't find sign = ");
        }catch (NoPropertiesException e){
            throw new NoPropertiesException("There is nothing to parse");
        } catch (IOException e) {
            throw new IOException("Can't read file", e);

        }
    }
    @Test
    public void getValueTest() throws NoKeyFoundException, NotAValidPathException, NotAValidFileFormatException, NoPropertiesException, IOException {
        try {
            HashMap<String, String> expectPairs = new HashMap<>();
            expectPairs.put("key", "value");
            KeyValueParser parser = new KeyValueParser(filePath);
            Assert.assertEquals("value", parser.getStringValue("key"));
        } catch (NotAValidPathException e){
            throw new NotAValidPathException("The path argument is not valid");
        } catch (NotAValidFileFormatException e){
            throw new NotAValidFileFormatException("The format of file is not valid.Can't find sign = ");
        }catch (NoPropertiesException e){
            throw new NoPropertiesException("There is nothing to parse");
        } catch (IOException e) {
            throw new IOException("Can't read file", e);

        }
    }

}