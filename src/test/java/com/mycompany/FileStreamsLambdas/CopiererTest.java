package com.mycompany.FileStreamsLambdas;
import org.junit.Assert;
import org.junit.Test;
import com.mycompany.FileStreamsLambdas.Exceptions.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CopiererTest {

    public final Path srcPath = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/source_file.txt");
    public final Path destPath = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/source_file_copy.txt");
    Copierer copierer = new Copierer();

    @Test
    public void  readAndWriteFileExistsTest() throws NotAValidPathException, IOException
    {
        try {
            copierer.readAndWriteFile(srcPath);
        }
        catch(NotAValidPathException e)
        {
            throw new NotAValidPathException("The path argument is not valid");
        } catch (DirectoryException e) {
            e.printStackTrace();
        }

        Assert.assertTrue(destPath.toFile().exists());
    }
    @Test
    public void  readAndWriteFileContentTest() throws Exception
    {
        StringBuilder  sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(destPath.toFile()))) {
            copierer.readAndWriteFile(srcPath);
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        }
        catch(NotAValidPathException ex)
        {
            throw new NotAValidPathException("The path argument is not valid");
        }


        Assert.assertEquals("some text", sb.toString());

    }

}