package com.mycompany.FileStreamsLambdas;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by asus on 6/8/17.
 */
public class CommandsTest {

    Commands cmd = new Commands();
    public final Path filePath1 = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/source_file_copy.txt");
    public final Path filePath2 = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/source_file.txt");
    public final Path filePath3 = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources/key-value.txt");
    public final Path filePath4 = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources2/doc.txt");
    public final Path filePath5 = Paths.get("src/main/java/com/mycompany/FileStreamsLambdas/resources2/words.txt");
    @Test
    public void lsTest()
    {

        List<File> list = cmd.ls("src/main/java/com/mycompany/FileStreamsLambdas/resources");

        Assert.assertTrue(list.contains(filePath1.toFile())&&list.contains(filePath2.toFile())&&list.contains(filePath3.toFile()));
    }
    @Test
    public void catTest() throws IOException {

        Assert.assertEquals("some text", cmd.cat(filePath2.toFile()));

    }
    @Test
    public void tailTest() throws IOException {

        String expect ="ghijklmnop";

        Assert.assertEquals(expect, cmd.tail(filePath4.toFile(), 10));

    }
    @Test
    public void wcTest() throws IOException {

        Assert.assertEquals(3, cmd.wc(filePath5.toFile()));

    }

}
