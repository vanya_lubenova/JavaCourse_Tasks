package com.mycompany.FileStreamsLambdas;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 6/27/17.
 */
@FunctionalInterface
public interface FuncInterface {
    ArrayList<Student> getStudents(ArrayList<Student> students);

}
