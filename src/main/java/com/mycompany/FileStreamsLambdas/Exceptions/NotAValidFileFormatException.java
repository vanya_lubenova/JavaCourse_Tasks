package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/7/17.
 */
public class NotAValidFileFormatException extends Exception {
    public NotAValidFileFormatException(String message)
    {
        super(message);
    }
}
