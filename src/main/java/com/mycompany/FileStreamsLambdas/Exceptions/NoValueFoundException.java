package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/8/17.
 */
public class NoValueFoundException extends Exception{
    public NoValueFoundException(String message)
    {
        super(message);
    }
}

