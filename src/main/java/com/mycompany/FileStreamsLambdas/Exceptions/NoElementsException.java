package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/8/17.
 */
public class NoElementsException extends Exception{
    public NoElementsException(String message)
    {
        super(message);
    }
}
