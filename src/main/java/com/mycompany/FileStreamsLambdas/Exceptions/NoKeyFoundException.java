package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/7/17.
 */
public class NoKeyFoundException extends Exception{
    public NoKeyFoundException(String message)
    {
        super(message);
    }
}
