package com.mycompany.FileStreamsLambdas.Exceptions;

public class NotAValidPathException extends Exception{

    public NotAValidPathException(String message)
    {
        super(message);
    }
}
