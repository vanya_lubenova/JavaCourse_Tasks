package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/27/17.
 */
public class DirectoryException extends Exception{
    public DirectoryException(String message)
    {
        super(message);
    }
}
