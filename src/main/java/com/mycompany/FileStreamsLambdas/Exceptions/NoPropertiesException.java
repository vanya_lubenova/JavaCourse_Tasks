package com.mycompany.FileStreamsLambdas.Exceptions;

/**
 * Created by asus on 6/7/17.
 */
public class NoPropertiesException extends Exception {
    public NoPropertiesException(String message)
    {
        super(message);
    }
}
