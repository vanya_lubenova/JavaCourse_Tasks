package com.mycompany.FileStreamsLambdas;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class Commands {

    public static List<File> ls(String directoryName) {
        File directory = new File(directoryName);
        List<File> resultList = new ArrayList<File>();
        File[] fList = directory.listFiles();
        resultList.addAll(Arrays.asList(fList));
        for (File file : fList) {
            if (file.isFile()) {
                System.out.println(file.getAbsolutePath());
            } else if (file.isDirectory()) {
                resultList.addAll(ls(file.getAbsolutePath()));
            }
        }
        return resultList;
    }
    public static String cat(File file) throws IOException {
        StringBuilder sb = new StringBuilder();
        try(BufferedReader in = new BufferedReader(new FileReader(file)))
        {
            String line;
            while((line = in.readLine()) != null)
            {
                sb.append(line);
                System.out.println(line);
            }
            in.close();
        }

        return sb.toString();
    }
    private static int linesInFile(File file) throws IOException {
        int count = 0;
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = br.readLine()) != null) {
                count++;

            }
        } catch (IOException e) {
            throw new IOException("File can not be readed");
        }

        return count;
    }

    public static String tail(File file, int lNumber) throws IOException {
        StringBuilder sb = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String line;
            int count = 0;
            while ((line = br.readLine()) != null) {
                count++;
                if (count >= linesInFile(file) - lNumber && count <= linesInFile(file))
                {
                    System.out.println(line);
                    sb.append(line.trim());
                }

            }

        } catch (IOException e) {
            throw new IOException("File can not be readed");
        }

        return sb.toString();

    }
    public static int wc(File file) throws IOException {
        int count = 0;;
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String line;
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line);
                count += st.countTokens();
            }

        } catch (IOException e) {
            throw new IOException("File can not be readed");
        }

        return count;


    }



}
