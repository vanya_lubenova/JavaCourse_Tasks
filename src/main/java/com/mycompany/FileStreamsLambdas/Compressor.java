package com.mycompany.FileStreamsLambdas;

import com.mycompany.FileStreamsLambdas.Exceptions.NoKeyFoundException;
import com.mycompany.FileStreamsLambdas.Exceptions.NoValueFoundException;

import java.io.*;
import java.util.*;


public class Compressor {
    private static final HashMap<String, Integer> pairs = new HashMap<String, Integer>();
    private static int number = 0;


    public Integer getValue(String key) throws NoKeyFoundException {
        if (!(pairs.containsKey(key))) {
            throw new NoKeyFoundException("File doesn't contain this key");
        }
        return this.pairs.get(key);
    }

    public String getKey(Character value) throws NoValueFoundException, NoKeyFoundException {
        Set keys = pairs.keySet();
        String resultKey = null;
        for (Object key : keys) {
            if (getValue(key.toString()) == Integer.parseInt(value.toString())) {
                resultKey = key.toString();
            }
        }

        return resultKey;
    }

    private static String encodeLine(String line, int number) {
        StringBuilder encodedLine = new StringBuilder();
        StringTokenizer st = new StringTokenizer(line);
        int numberOfWords = st.countTokens();
        for (int i = 0; i < numberOfWords; i++) {
            encodedLine.append(number);
            pairs.put(st.nextToken(), number);
            number++;
        }
        return encodedLine.toString();

    }

    private String decodeLine(String line) throws NoValueFoundException, NoKeyFoundException {
        StringBuilder decodedLine = new StringBuilder();
        for (int i = 0; i < line.length(); i++) {
            if(i == line.length() - 1){
                decodedLine.append(getKey(line.charAt(i)));

            }
            else {
                decodedLine.append(getKey(line.charAt(i)));
                decodedLine.append(" ");
            }
        }
        return decodedLine.toString();

    }

    public static void encode(File file) throws IOException {
        String filePath = file.getPath();
        String path = filePath.toString().substring(0, filePath.toString().lastIndexOf("."));
        String extension = filePath.substring(filePath.toString().lastIndexOf("."));
        try (BufferedReader br = new BufferedReader(new FileReader(file));
             BufferedWriter bw = new BufferedWriter((new FileWriter(path + "_encoded" + extension)))) {
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(encodeLine(line, number));
            }

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File is not found");
        } catch (IOException e) {
            throw new IOException("File can not be readed");
        }
    }

    public void decode(File file) throws IOException, NoValueFoundException, NoKeyFoundException {
        String absPath = file.getPath();
        String path = absPath.toString().substring(0, absPath.toString().lastIndexOf("_"));
        String extension = absPath.substring(absPath.toString().lastIndexOf("."));
        if(!(new File(path + "_encoded" + extension).exists())) { throw new FileNotFoundException("File is not encoded yet");}
        try (BufferedReader br = new BufferedReader(new FileReader(path + "_encoded" + extension ));
             BufferedWriter bw = new BufferedWriter((new FileWriter(path + "_decoded" + extension)))) {
            String line;
            while ((line = br.readLine()) != null) {
                bw.write(decodeLine(line));
            }

        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File is not found");
        } catch (IOException e) {
            throw new IOException("File can not be readed");
        } catch (NoValueFoundException e) {
            throw new NoValueFoundException("Value is not found");
        } catch (NoKeyFoundException e) {
            throw new NoKeyFoundException("Key is not found");
        }

    }
}
