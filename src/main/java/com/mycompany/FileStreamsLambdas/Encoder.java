package com.mycompany.FileStreamsLambdas;

import java.nio.charset.StandardCharsets;
import java.io.*;


public class Encoder {
    private static final String suffix = "_copy";

    public static void fixEncoding(File source) throws IOException {
        String sourcePath = source.getPath();
        String directory = sourcePath.substring(0, sourcePath.toString().lastIndexOf("."));
        String extension = source.getPath().toString().substring(sourcePath.toString().lastIndexOf("."));

        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(sourcePath), "windows-1251"));
             BufferedWriter out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(directory + suffix + extension), StandardCharsets.UTF_8.name()))) {
            String line;
            while ((line = in.readLine()) != null) {
                out.write(line);
            }
        } catch (UnsupportedEncodingException e) {
            throw new UnsupportedEncodingException("The encoding is not supported");
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("File is not found");
        } catch (IOException e) {
            throw new IOException("File can not be readed");
        }
    }
}
