package com.mycompany.FileStreamsLambdas;

import com.mycompany.FileStreamsLambdas.Exceptions.DirectoryException;
import com.mycompany.FileStreamsLambdas.Exceptions.NotAValidPathException;

import java.io.*;
import java.nio.file.Path;


public class Copierer {
    private static final String suffix = "_copy";

    /**
     * @pre filePath != null
     */
    public static final File readAndWriteFile(Path filePath) throws NotAValidPathException, IOException, DirectoryException {
        if (!(filePath.toFile().exists())) {
            throw new NotAValidPathException("The path argument is not valid");
        }
        if (filePath.toFile().isDirectory()) {
            throw new DirectoryException("File argument is a directory");
        }
            String srcPath = filePath.toString().substring(0, filePath.toString().lastIndexOf("."));
            String extension = filePath.toString().substring(filePath.toString().lastIndexOf("."));
            String trgPath = srcPath + suffix + extension;
            try (BufferedReader br = new BufferedReader(new FileReader(filePath.toFile()));
                 BufferedWriter bw = new BufferedWriter((new FileWriter(trgPath)))) {
                String line;
                while ((line = br.readLine()) != null) {
                    bw.write(line);
                }

            }
            return new File(trgPath);
        }
    }


