package com.mycompany.FileStreamsLambdas;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.mycompany.FileStreamsLambdas.Exceptions.*;
/**
 * @invariant filePath != null
 */
public class KeyValueParser {

    private final Path filePath;
    private final HashMap<String, String> keyValuePairs;

    /**
     * @pre filePath != null
     */
    public KeyValueParser(Path filePath) throws IOException, NoPropertiesException, NotAValidFileFormatException, NotAValidPathException {
        if(!(filePath.toFile().exists())){ throw new NotAValidPathException("The path argument is not valid");}
        this.filePath = filePath;
        this.keyValuePairs = new HashMap<>();
        this.readFile();
    }
    /**
     * @post keyValuePairs.size() > 0
     */
    private void readFile() throws IOException, NotAValidFileFormatException, NoPropertiesException {
        try (BufferedReader br = new BufferedReader(new FileReader(this.filePath.toFile()))) {
            String line;
            while ((line = br.readLine()) != null) {
                this.parse(line);
            }
        } catch (IOException e){
            throw new IOException("Can't read file", e);
        }
        catch (NotAValidFileFormatException e)
        {
            throw new NotAValidFileFormatException("The format of file is not valid.Can't find sign = ");
        }
        catch(NoPropertiesException e)
        {
            throw new NoPropertiesException("There is nothing to parse");
        }
    }
    private void parse(String line) throws NotAValidFileFormatException, NoPropertiesException{
        int indexOfEqual = line.indexOf("=");

        if (indexOfEqual == -1) {
            throw new NotAValidFileFormatException("The format is not valid.Can't find sign = ");
        }

        String key = line.substring(0, indexOfEqual).trim();
        String value = line.substring(indexOfEqual + 1).trim();

        if (key.isEmpty() || value.isEmpty()) {
            throw new NoPropertiesException("There is nothing to parse");
        }

        this.keyValuePairs.put(key, value);
    }
    /**
     * @pre key != null
     */
    public String getStringValue(String key) throws NoKeyFoundException {
        if(!(keyValuePairs.containsKey(key))){ throw new NoKeyFoundException("File doesn't contain this key");}
        return this.keyValuePairs.get(key);
    }
    public int getIntValue(String key) throws NoKeyFoundException {
        if(!(keyValuePairs.containsKey(key))){ throw new NoKeyFoundException("File doesn't contain this key");}
        return Integer.parseInt(this.keyValuePairs.get(key));
    }
    public Double getDoubleValue(String key) throws NoKeyFoundException {
        if(!(keyValuePairs.containsKey(key))){ throw new NoKeyFoundException("File doesn't contain this key");}
        return Double.parseDouble(this.keyValuePairs.get(key));
    }
    public HashMap<String, String> getKeyValuePairs() throws NoPropertiesException{

        if(this.keyValuePairs.isEmpty()){throw new NoPropertiesException("There is no key-value pairs");}
        return this.keyValuePairs;
    }


}
