package com.mycompany.FileStreamsLambdas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * Created by asus on 6/8/17.
 */
public class SlidingWindowSpliterator implements  Spliterator{

    private Collection<String> partitionedCollection;

    private int slidingLength;

    public SlidingWindowSpliterator(Collection<String> collection, int slidingLength)
    {
        this.slidingLength = slidingLength;
        partitioning(collection, slidingLength);
    }

    private void partitioning(Collection<String> collection, int slidingLength)
    {
        int countSlides = 0;
        ArrayList<String> sb = new ArrayList<String>();
            for (String elem : collection) {
               if(countSlides / slidingLength == 0 && sb.size() != 0)
               {
                   sb.add(elem);
                   partitionedCollection.add(sb.toString());
                   sb.removeAll(sb);
               }
               else
               {
                   sb.add(elem + " ");
               }

            }
    }


    @Override
    public boolean tryAdvance(Consumer action) {
        return false;
    }

    @Override
    public Spliterator trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return 0;
    }
}
