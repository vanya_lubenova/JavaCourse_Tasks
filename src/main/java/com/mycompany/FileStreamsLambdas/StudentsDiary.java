package com.mycompany.FileStreamsLambdas;

import com.mycompany.FileStreamsLambdas.Exceptions.NoElementsException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 6/8/17.
 */
public class StudentsDiary {

    public double getAverageMark(ArrayList<Student> students) {
        double average = 0.0;
        for (int i = 0; i < students.size(); i++) {

            average += students.get(i).getMark();

        }

        return average / students.size();
    }

    public ArrayList<Student> getAllPassing(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> allPassing = new ArrayList<>();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getMark() >= 3.0) {
                allPassing.add(students.get(i));
            }
        }

        return allPassing;
    }

    public ArrayList<Student> getAllFailing(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> allFailing = new ArrayList<>();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getMark() < 3.0) {
                allFailing.add(students.get(i));
            }
        }

        return allFailing;
    }
    private boolean contains(Student student, ArrayList<Student> students) {
        boolean flag = false;
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).equal(student)) {
                flag = true;
            }
        }

        return flag;
    }
    /**
     * @pre students.size() > 0
     */
    public ArrayList<Student> orderByMarksAscending(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> ordered = new ArrayList<>();
        int count = 0;
        while (count < students.size()) {
            Student min = students.get(0);
            for (int i = 1; i < students.size(); i++) {
                if (students.get(i).getMark() <= min.getMark() && !(contains(students.get(i), ordered))) {
                    min = students.get(i);
                }
            }
            ordered.add(min);
            count++;
        }

        return ordered;
    }

    public ArrayList<Student> orderByMarksDescending(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> ordered = new ArrayList<>();
        int count = 0;
        while (count < students.size()) {
            Student max = students.get(0);
            for (int i = 1; i < students.size(); i++) {
                if (students.get(i).getMark() >= max.getMark() && !(contains(students.get(i), ordered))) {
                    max = students.get(i);
                }
            }
            ordered.add(max);
            count++;
        }

        return ordered;
    }
    private ArrayList<Student> equalMarkStudents(ArrayList<Student> students, double mark) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> equal = new ArrayList<>();
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getMark() == mark) {
                equal.add(students.get(i));
            }
        }

        return equal;
    }
    public ArrayList<Student> highestMarkStudents(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> highest = new ArrayList<>();
        if (students.size() == 0) {
            throw new ArrayIndexOutOfBoundsException("No students founded");
        }
        Student max = students.get(0);
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getMark() > max.getMark()) {
                max = students.get(i);
            }
        }

        return equalMarkStudents(students, max.getMark());
    }
    public ArrayList<Student> lowestMarkStudents(ArrayList<Student> students) throws NoElementsException {
        if(students.size() == 0)
        {
            throw new NoElementsException("No students founded");
        }
        ArrayList<Student> highest = new ArrayList<>();
        if (students.size() == 0) {
            throw new ArrayIndexOutOfBoundsException("No students founded");
        }
        Student min = students.get(0);
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getMark() < min.getMark()) {
                min = students.get(i);
            }
        }

        return equalMarkStudents(students, min.getMark());
    }
    public List<Double> marksDistributionByAge(ArrayList<Student> students, int age)
    {
        List<Double> marks = new ArrayList<>();
        if (students.size() == 0) {
            throw new ArrayIndexOutOfBoundsException("No students founded");
        }
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getAge() == age)
            {
                marks.add(students.get(i).getMark());
            }
        }
        return marks;
    }
    public double getAverageMarkByGender(ArrayList<Student> students, boolean gender)
    {
        double average = 0.0;
        if (students.size() == 0) {
            throw new ArrayIndexOutOfBoundsException("No students founded");
        }
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getGender() == gender)
            {
                average += students.get(i).getMark();
            }
        }
        return average / students.size();
    }
    public List<Double> marksDistributionByGender(ArrayList<Student> students, boolean gender)
    {
        List<Double> marks = new ArrayList<>();
        if (students.size() == 0) {
            throw new ArrayIndexOutOfBoundsException("No students founded");
        }
        for (int i = 0; i < students.size(); i++) {
            if (students.get(i).getGender() == gender)
            {
                marks.add(students.get(i).getMark());
            }
        }
        return marks;
    }

}
