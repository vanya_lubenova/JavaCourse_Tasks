package com.mycompany.FileStreamsLambdas;

/**
 * Created by asus on 6/8/17.
 */
public class Student {
    private String name;
    private double mark;
    private int age;
    private boolean gender;
    public Student(String name, double mark, int age)
    {
        this.name = name;
        this.mark = mark;
        this.age = age;
    }
    public boolean getGender()
    {
        return this.gender;
    }
    public int getAge()
    {
        return this.age;
    }
    public String getName()
    {
        return this.name;
    }
    public double getMark()
    {
        return this.mark;
    }
    public boolean equal(Student other)
    {
        boolean flag = false;
        if(this.getMark() == other.getMark() && this.name.equals(other.name))
        {
            flag = true;
        }

        return flag;
    }
}
