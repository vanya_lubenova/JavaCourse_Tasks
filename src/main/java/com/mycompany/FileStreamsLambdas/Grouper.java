package com.mycompany.FileStreamsLambdas;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Paths;
/**
 * Created by asus on 6/8/17.
 */
public class Grouper {
    private static HashMap<String, List<File>> list;

    public static void findDuplicatedFiles(String directoryName) throws IOException {
        File directory = new File(directoryName);
        File[] fList = directory.listFiles();
        for (File file : fList) {
            if (file.getName().charAt(0) == '.') {
                continue;
            }
            if (file.isFile()) {
                if(list.containsKey(Files.readAllBytes(file.toPath()).toString()))
                {
                    list.get(Files.readAllBytes(file.toPath()).toString()).add(file);
                }
                else
                {
                    List<File> files = Arrays.asList(file);
                    list.put(Files.readAllBytes(file.toPath()).toString(), files);
                }
            } else if (file.isDirectory()) {
               findDuplicatedFiles(file.getAbsolutePath());
            }
        }
    }
    public void printHashMap()
    {
        for (List<File> list : list.values()) {
            if (list.size() > 1) {
                System.out.println("\n");
                for (File file : list) {
                    System.out.println(file);
                }
            }
        }
    }
}
