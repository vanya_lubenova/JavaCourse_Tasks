package com.mycompany.Exceptions.task2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by asus on 5/23/17.
 */
public class SensorManager {
    private String[] sensorTypes = {"weather", "flood", "air quality"};
    List<Sensor> sensors = new ArrayList<Sensor>();

    public void registerSensor(Sensor s) throws SensorRegisteredException, UnknownSensorException{

        if (searchSensorInRegister(s) == true)
        {
            throw new SensorRegisteredException("Sensor is already registered.");
        }
        if (validType(s) == false)
        {
            throw new UnknownSensorException("Unknown sensor type");
        }


        sensors.add(s);
    };
    public void unRegisterSensor(Sensor s) throws SensorUnregisteredException{
        if (searchSensorInRegister(s) == false)
        {
            throw new SensorUnregisteredException("Sensor is unregistered.");
        }

        sensors.remove(s);

    };
    public boolean searchSensorInRegister(Sensor s){
        return false;
    };
    public boolean validType(Sensor s){
        return true;
    };
}
