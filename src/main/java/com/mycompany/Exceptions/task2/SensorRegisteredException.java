package com.mycompany.Exceptions.task2;

/**
 * Created by asus on 5/23/17.
 */
public class SensorRegisteredException extends Exception{

    public SensorRegisteredException(String message)
    {
        super(message);
    }

}
