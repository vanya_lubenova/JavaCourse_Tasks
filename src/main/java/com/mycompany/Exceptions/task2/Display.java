package com.mycompany.Exceptions.task2;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by asus on 5/23/17.
 */
public class Display extends TimerTask {
    public void run()
    {
        SensorManager manager = new SensorManager();
        Sensor s = new Sensor("flood");
        try {
            manager.registerSensor(s);
        } catch (SensorRegisteredException sensorRegistered) {
            sensorRegistered.printStackTrace();
        }
        catch (UnknownSensorException unknownSensor) {
           unknownSensor.printStackTrace();
        }
        try {
            s.read();
        } catch (SensorUnreachableException sensorUnreachable) {
            sensorUnreachable.printStackTrace();
        }
    }


    public static void main(String[] args) {
        Timer timer = new Timer();
        timer.schedule(new Display(), 0, 30000);
    }
}
