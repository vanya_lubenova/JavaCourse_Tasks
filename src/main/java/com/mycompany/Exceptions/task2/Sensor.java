package com.mycompany.Exceptions.task2;

/**
 * Created by asus on 5/23/17.
 */
public class Sensor {
    private String sensorType;

    Sensor(String sensorType)
    {
        this.sensorType = sensorType;
    }

    public String getSensorType()
    {
        return sensorType;
    }

    public void read() throws SensorUnreachableException{
        System.out.println("Sensor data:" + this.sensorType);
        //if occurs network error
        //throw new SensorUnreachable("Sensor is unreachable.");
    }


}
