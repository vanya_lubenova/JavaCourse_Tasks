package com.mycompany.Exceptions.task2;

/**
 * Created by asus on 5/23/17.
 */
public class UnknownSensorException extends Exception {

    public UnknownSensorException(String message)
    {
        super(message);
    }

}
