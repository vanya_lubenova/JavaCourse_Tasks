package com.mycompany.Exceptions.task2;

/**
 * Created by asus on 5/23/17.
 */
public class SensorUnreachableException extends Exception {

    public SensorUnreachableException(String message)
    {
        super(message);
    }

}
