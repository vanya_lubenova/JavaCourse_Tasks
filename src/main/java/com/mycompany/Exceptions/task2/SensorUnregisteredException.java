package com.mycompany.Exceptions.task2;

/**
 * Created by asus on 5/23/17.
 */
public class SensorUnregisteredException extends Exception{

    public SensorUnregisteredException(String message)
    {
        super(message);
    }

}
