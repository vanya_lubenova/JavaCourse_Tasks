package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 6/5/17.
 */
public class Location {
    private String name;
    private int coordinates;

    Location(String name,int coordinates)
    {
        this.name = name;
        this.coordinates = coordinates;
    }
    int getCoordinates()
    {
        return this.coordinates;
    }
    String getName()
    {
        return this.name;
    }


}
