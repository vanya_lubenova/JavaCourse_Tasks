package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 5/23/17.
 */
public class TimeOutException extends Exception {

    public TimeOutException(String message)
    {
        super(message);
    }

}