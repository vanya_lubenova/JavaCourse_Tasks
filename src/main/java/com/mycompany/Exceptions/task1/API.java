package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 5/23/17.
 */
public class API {
    public String getWeather(Location location) throws NoDataException, NoLocationFoundException, NoNetworkException, TimeOutException {
        //getting data
        if (location.getCoordinates() == 1) {
            throw new NoDataException("No data found for " + location.getName() + " (" + location.getCoordinates() + ")");
        }
        if (location.getCoordinates() == 2) {
            throw new NoLocationFoundException("Location"+ location.getName() + " (" + location.getCoordinates() + ")" + "doesn't exist.");
        }
        if (location.getCoordinates() == 3) {
            throw new NoNetworkException("No connection with the weather service.");
        }
        if (location.getCoordinates() == 4) {
            throw new TimeOutException("The weather service has taken too long to respond.");
        }

        return "data";

    }

    public static void main(String[] args) {

    }

}
