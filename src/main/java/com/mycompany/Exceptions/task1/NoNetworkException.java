package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 5/23/17.
 */
public class NoNetworkException extends Exception {

    public NoNetworkException(String message)
    {
        super(message);
    }

}