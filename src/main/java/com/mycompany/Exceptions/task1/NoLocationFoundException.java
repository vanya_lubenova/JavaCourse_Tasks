package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 5/23/17.
 */
public class NoLocationFoundException extends Exception {

    public NoLocationFoundException(String message)
    {
        super(message);
    }

}