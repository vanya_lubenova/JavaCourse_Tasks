package com.mycompany.Exceptions.task1;

/**
 * Created by asus on 5/23/17.
 */
public class NoDataException extends Exception{

    public NoDataException(String message)
    {
        super(message);
    }

}